package eb_tech_solutions.backend.spring.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eb_tech_solutions.backend.spring.model.ContactUsRequest;
import eb_tech_solutions.backend.spring.repository.ContactUsRepository;

@Service
public class ContactUsService {
	
    private String status;
    private String message;
    protected HashMap map = new HashMap();
    protected ContactUsRequest contactUsRequest;
    
	@Autowired
    private final ContactUsRepository contactUsRepository;
	
	@Autowired
    public ContactUsService(ContactUsRepository contactUsRepository) {
        this.contactUsRepository = contactUsRepository;
    }
	
	 public HashMap validateAddContactUsRequest(ContactUsRequest contactUsRequest) {
	    	if(contactUsRequest != null) {
	    	 	   contactUsRepository.save(new ContactUsRequest(contactUsRequest.getName(), contactUsRequest.getEmail(),contactUsRequest.getSubject(),
	    	 			   contactUsRequest.getMessage(),contactUsRequest.getStatus(), contactUsRequest.getResponse()));
	    	 	 	     message = "ContactUs Request added Successful";
	    	 	          status = "success";
	    	}else {
	    		  message = "ContactUs Request added Unsuccessful";
	 	          status = "fail";
	    	}

	        // values for angualar
	        map.put("status", status);
	        map.put("message", message);
	        return map;

	    }

}
