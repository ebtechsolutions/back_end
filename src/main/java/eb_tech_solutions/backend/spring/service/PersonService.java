package eb_tech_solutions.backend.spring.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eb_tech_solutions.backend.spring.model.Person;
import eb_tech_solutions.backend.spring.repository.PersonRepository;

@Service
public class PersonService {
	
    private String status;
    private String message;
    protected HashMap map = new HashMap();
    protected Person customer;
    
	@Autowired
    private final PersonRepository personRepository;
	
	@Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }
	
    public HashMap validateUser(Person user) {

        user = personRepository.getByUsernameAndPassword(user.getUsername(), user.getPassword());
        //Check if returned value is not null
        if(user != null) {
        if (user.getUsername().equals(user.getUsername()) && user.getPassword().equals(user.getPassword())) {
            message = "Login Successful";
            status = "success";
            
        } else {
            message = "Login Unsuccessful";
            status = "fail";
        }
       }else {
    	   message = "Login Unsuccessful";
           status = "fail"; 
       }
        // values for angualar
        map.put("status", status);
        map.put("message", message);
        return map;
    }	

    public HashMap validateRegUser(Person user) {

    	Person returnUser = personRepository.findByUsername(user.getUsername());
        //Check if returned value is not null
        if(returnUser != null) {
        if (returnUser.getUsername().equals(user.getUsername())) {
            message = "Login Unsuccessful";
            status = "fail";
            
        } else {
     	   personRepository.save(new Person(user.getName(), user.getSurname(),
    	   user.getIdNo(), user.getPassPortNo(), user.getEmail(), user.getUsername(),user.getPassword()));
            message = "Register Successful";
            status = "success";
        }
       }else {
    	   personRepository.save(new Person(user.getName(), user.getSurname(),
    			   user.getIdNo(), user.getPassPortNo(), user.getEmail(), user.getUsername(),user.getPassword()));
    	     message = "Register Successful";
             status = "success";
       }
        // values for angualar
        map.put("status", status);
        map.put("message", message);
        return map;
    }
}
