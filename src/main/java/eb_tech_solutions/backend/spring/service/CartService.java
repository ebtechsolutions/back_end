package eb_tech_solutions.backend.spring.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eb_tech_solutions.backend.spring.model.Cart;
import eb_tech_solutions.backend.spring.repository.CartRepository;

@Service
public class CartService {
	
    private String status;
    private String message;
    protected HashMap map = new HashMap();
    protected Cart cart;
    
	@Autowired
    private final CartRepository cartRepository;
	
	@Autowired
    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }
	
    public HashMap validateAddCart(Cart cart) {
    	if(cart != null) {
    	 	   cartRepository.save(new Cart(cart.getLoggedInUser(), cart.getServiceType(),cart.getPackageType(),
    	 			   cart.getPackageFormat(),cart.getPageQuantity(), cart.getPackageCost(), 
    	 			   cart.getPackageStatus(),cart.getPackageDscrptn()));
    	 	 	     message = "Cart added Successful";
    	 	          status = "success";
    	}else {
    		  message = "Cart added Unsuccessful";
 	          status = "fail";
    	}

        // values for angualar
        map.put("status", status);
        map.put("message", message);
        return map;

    }
 
}
