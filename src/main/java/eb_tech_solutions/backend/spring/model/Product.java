package eb_tech_solutions.backend.spring.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @NotNull
    private String prodName;    
    
    private String prodPrice;
    
    private String prodDescription;
    
    private String prodSLA;
    
    private String prodDiscount;
    
    private String prodType;
    
    public Product() {
    }

    public Product(@NotNull String prodName, String prodPrice, String prodDescription, 
    		String prodSLA, String prodDiscount, String prodType) {
        this.prodName = prodName;
        this.prodPrice = prodPrice;
        this.prodDescription = prodDescription;
        this.prodSLA = prodSLA;
        this.prodDiscount = prodDiscount;
        this.prodType = prodType;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(String prodPrice) {
		this.prodPrice = prodPrice;
	}

	public String getProdDescription() {
		return prodDescription;
	}

	public void setProdDescription(String prodDescription) {
		this.prodDescription = prodDescription;
	}

	public String getProdSLA() {
		return prodSLA;
	}

	public void setProdSLA(String prodSLA) {
		this.prodSLA = prodSLA;
	}

	public String getProdDiscount() {
		return prodDiscount;
	}

	public void setProdDiscount(String prodDiscount) {
		this.prodDiscount = prodDiscount;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	 
    
}
