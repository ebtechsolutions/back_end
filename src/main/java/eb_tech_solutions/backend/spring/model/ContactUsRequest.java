package eb_tech_solutions.backend.spring.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "contactUsRequest")
public class ContactUsRequest {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String name;
    
    @NotNull
    private String email;
    
    private String subject;
    
    private String message;
    
    @NotNull 
    private String status;
    
    private String response;
    
    public ContactUsRequest() {
    }

    public ContactUsRequest(@NotNull String name, @NotNull String email, String subject, String message, 
    		@NotNull String status, String response) {
        this.name = name;
        this.email = email;
        this.subject = subject;
        this.message = message;
        this.status = status;
        this.response = response;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
    
    

}
