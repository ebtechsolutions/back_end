package eb_tech_solutions.backend.spring.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cart")
public class Cart {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	private String loggedInUser;

	private String serviceType;

	private String packageType;

	private String packageFormat;

	private String pageQuantity;

	private String packageCost;

	private String packageStatus;

	private String packageDscrptn;
	
	
	public Cart() {
		
	}

    public Cart( @NotNull String loggedInUser, String serviceType, String packageType, String packageFormat, 
    		String pageQuantity, String packageCost, String packageStatus , String packageDscrptn) {
        this.loggedInUser = loggedInUser;
        this.serviceType = serviceType;
        this.packageType = packageType;
        this.packageFormat = packageFormat;
        this.pageQuantity = pageQuantity;
        this.packageCost = packageCost;
        this.packageStatus = packageStatus;
        this.packageDscrptn = packageDscrptn;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getPackageType() {
		return packageType;
	}

	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}

	public String getPackageFormat() {
		return packageFormat;
	}

	public void setPackageFormat(String packageFormat) {
		this.packageFormat = packageFormat;
	}

	public String getPageQuantity() {
		return pageQuantity;
	}

	public void setPageQuantity(String pageQuantity) {
		this.pageQuantity = pageQuantity;
	}

	public String getPackageCost() {
		return packageCost;
	}

	public void setPackageCost(String packageCost) {
		this.packageCost = packageCost;
	}

	public String getPackageStatus() {
		return packageStatus;
	}

	public void setPackageStatus(String packageStatus) {
		this.packageStatus = packageStatus;
	}

	public String getPackageDscrptn() {
		return packageDscrptn;
	}

	public void setPackageDscrptn(String packageDscrptn) {
		this.packageDscrptn = packageDscrptn;
	}
    
    
}
