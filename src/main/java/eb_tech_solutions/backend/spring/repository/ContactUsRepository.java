package eb_tech_solutions.backend.spring.repository;

import org.springframework.data.repository.CrudRepository;

import eb_tech_solutions.backend.spring.model.ContactUsRequest;


public interface ContactUsRepository extends CrudRepository<ContactUsRequest, Long>{
	public ContactUsRequest findByStatus(String status);	

}
