package eb_tech_solutions.backend.spring.repository;

import org.springframework.data.repository.CrudRepository;

import eb_tech_solutions.backend.spring.model.Cart;


public interface CartRepository extends CrudRepository<Cart, Long>{
	public Cart findByLoggedInUser(String loggedInUser);
}
