package eb_tech_solutions.backend.spring.repository;


import org.springframework.data.repository.CrudRepository;

import eb_tech_solutions.backend.spring.model.Person;

public interface PersonRepository extends CrudRepository<Person, Long> {
	public Person findByUsername(String username);
	public Person getByUsernameAndPassword (String username, String password);

}
