package eb_tech_solutions.backend.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eb_tech_solutions.backend.spring.model.Product;


public interface ProductRepository extends JpaRepository<Product, Integer>{

}
