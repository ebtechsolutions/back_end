package eb_tech_solutions.backend.spring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eb_tech_solutions.backend.spring.model.ContactUsRequest;
import eb_tech_solutions.backend.spring.repository.ContactUsRepository;
import eb_tech_solutions.backend.spring.service.ContactUsService;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ContactUsController {
	
	@Autowired
	ContactUsRepository contactUsRepository;
	
	@Autowired
	private ContactUsService contactUsService;
	
	  @GetMapping("/contactUs")
	  public List<ContactUsRequest> getAllcontactUs() {
	    System.out.println("Get all contactUs...");
	 
	    List<ContactUsRequest> contactUs = new ArrayList<>();
	    contactUsRepository.findAll().forEach(contactUs::add);
	 
	    return contactUs;
	  }
	  
	  @DeleteMapping("/contactUs/deleteById{id}")
	  public ResponseEntity<String> deleteContactUs(@PathVariable("id") long id) {
	    System.out.println("Delete ContactUs request with ID = " + id + "...");
	 
	    contactUsRepository.deleteById(id);
	 
	    return new ResponseEntity<>("ContactUs request has been deleted!", HttpStatus.OK);
	  }
	  
	  
  	  @DeleteMapping("/contactUs/delete")
  public ResponseEntity<String> deleteAllContactUs() {
    System.out.println("Delete All ContactUs request...");
 
    contactUsRepository.deleteAll();
 
    return new ResponseEntity<>("All contactUs have been deleted!", HttpStatus.OK);
  }
  	  
 
	  @PutMapping("/contactUs/{id}")
	  public ResponseEntity<ContactUsRequest> updateContactUsRequest(@PathVariable("id") long id, @RequestBody ContactUsRequest contactUsRequest) {
	    System.out.println("Update ContactUsRequest with ID = " + id + "...");
	 
	    Optional<ContactUsRequest> contactUsRequestData = contactUsRepository.findById(id);
	 
	    if (contactUsRequestData.isPresent()) {
	      ContactUsRequest _contactUsRequest = contactUsRequestData.get();
	      _contactUsRequest.setName(contactUsRequest.getName());
	      return new ResponseEntity<>(contactUsRepository.save(_contactUsRequest), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	  
	  @PostMapping(value = "/contactUs/create")
	  public HashMap postContactUsRequest(@RequestBody ContactUsRequest contactUsRequest) {
		  HashMap map = contactUsService.validateAddContactUsRequest(contactUsRequest);
	    return map;
	  }
	  
	  @GetMapping(value = "contactUs/findByStatus/{status}")
	  public ContactUsRequest findByStatus(@PathVariable String status) {
	  	 
	  	    ContactUsRequest contactUs = contactUsRepository.findByStatus(status);
	  	    return contactUs;
	  	  }

}
