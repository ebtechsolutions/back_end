package eb_tech_solutions.backend.spring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eb_tech_solutions.backend.spring.model.Cart;
import eb_tech_solutions.backend.spring.repository.CartRepository;
import eb_tech_solutions.backend.spring.service.CartService;



@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class CartController {
	
	@Autowired
    CartRepository cartRepository;
	
	@Autowired
	private CartService cartService;
	
	  @GetMapping("/carts")
	  public List<Cart> getAllCarts() {
	    System.out.println("Get all Carts...");
	 
	    List<Cart> carts = new ArrayList<>();
	    cartRepository.findAll().forEach(carts::add);
	 
	    return carts;
	  }
	  
	  @DeleteMapping("/carts/deleteById{id}")
	  public ResponseEntity<String> deleteCart(@PathVariable("id") long id) {
	    System.out.println("Delete Cart with ID = " + id + "...");
	 
	    cartRepository.deleteById(id);
	 
	    return new ResponseEntity<>("Cart has been deleted!", HttpStatus.OK);
	  }
	  
	  
  	  @DeleteMapping("/carts/delete")
  public ResponseEntity<String> deleteAllcarts() {
    System.out.println("Delete All carts...");
 
    cartRepository.deleteAll();
 
    return new ResponseEntity<>("All carts have been deleted!", HttpStatus.OK);
  }
  	  
 
	  @PutMapping("/carts/{id}")
	  public ResponseEntity<Cart> updatecart(@PathVariable("id") long id, @RequestBody Cart cart) {
	    System.out.println("Update cart with ID = " + id + "...");
	 
	    Optional<Cart> cartData = cartRepository.findById(id);
	 
	    if (cartData.isPresent()) {
	      Cart _cart = cartData.get();
	      _cart.setLoggedInUser(cart.getLoggedInUser());
	      return new ResponseEntity<>(cartRepository.save(_cart), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	  
	  @PostMapping(value = "/carts/create")
	  public HashMap postCart(@RequestBody Cart cart) {
		  HashMap map = cartService.validateAddCart(cart);
	    return map;
	  }
	  
	  @GetMapping(value = "carts/username/{loggedInUser}")
	  public Cart findByLoggedInUser(@PathVariable String loggedInUser) {
	  	 
	  	    Cart carts = cartRepository.findByLoggedInUser(loggedInUser);
	  	    return carts;
	  	  }

}
