package eb_tech_solutions.backend.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import eb_tech_solutions.backend.spring.model.Product;
import eb_tech_solutions.backend.spring.repository.ProductRepository;

@CrossOrigin(origins = "*")
@RestController
public class ProductController {
	
	@Autowired
	ProductRepository productRepository;
	
   @CrossOrigin(origins = "*")
   @RequestMapping(
           method = RequestMethod.GET,
           path = "/getAllProducts",
           produces = MediaType.APPLICATION_JSON_VALUE
   )
   public List<Product> getAllProducts(){
       return productRepository.findAll();
   }
    
    @CrossOrigin(origins = "*")
    @RequestMapping(
            method = RequestMethod.POST,
            path = "/product",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Product addProduct(@RequestBody Product product) {
        return productRepository.save(product);
    }


}
