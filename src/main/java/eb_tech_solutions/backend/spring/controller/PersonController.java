package eb_tech_solutions.backend.spring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eb_tech_solutions.backend.spring.model.Person;
import eb_tech_solutions.backend.spring.repository.PersonRepository;
import eb_tech_solutions.backend.spring.service.PersonService;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PersonController {
	
	@Autowired
    PersonRepository personRepository;
	
	   @Autowired
	    private PersonService personService;

	  @GetMapping("/persons")
	  public List<Person> getAllPersons() {
	    System.out.println("Get all Persons...");
	 
	    List<Person> persons = new ArrayList<>();
	    personRepository.findAll().forEach(persons::add);
	 
	    return persons;
	  }
	 
	  @DeleteMapping("/persons/{id}")
	  public ResponseEntity<String> deletePerson(@PathVariable("id") long id) {
	    System.out.println("Delete Person with ID = " + id + "...");
	 
	    personRepository.deleteById(id);
	 
	    return new ResponseEntity<>("Person has been deleted!", HttpStatus.OK);
	  }
	 
	  @DeleteMapping("/persons/delete")
	  public ResponseEntity<String> deleteAllPersons() {
	    System.out.println("Delete All Persons...");
	 
	    personRepository.deleteAll();
	 
	    return new ResponseEntity<>("All persons have been deleted!", HttpStatus.OK);
	  }
	 
	  @GetMapping(value = "persons/username/{username}")
	  public Person findByUsername(@PathVariable String username) {
	 
	    Person persons = personRepository.findByUsername(username);
	    return persons;
	  }
	 
	  @PutMapping("/persons/{id}")
	  public ResponseEntity<Person> updatePerson(@PathVariable("id") long id, @RequestBody Person person) {
	    System.out.println("Update Person with ID = " + id + "...");
	 
	    Optional<Person> personData = personRepository.findById(id);
	 
	    if (personData.isPresent()) {
	      Person _person = personData.get();
	      _person.setName(person.getName());
	      return new ResponseEntity<>(personRepository.save(_person), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	  
	    @RequestMapping(value = "/persons/login", method = RequestMethod.POST)
	    public HashMap login(@RequestBody Person user) {
	        HashMap map = personService.validateUser(user);
	        return map;
	    } 
	    
		 
		  @PostMapping(value = "/persons/create")
		  public HashMap postPerson(@RequestBody Person person) {
			  HashMap map = personService.validateRegUser(person);
		    return map;
		  }
}
